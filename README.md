# Toy Robot - Refactoring Project

**Refactoring Goal:** Multi robot support

**Rules:**

- Robots can’t fall off the edges
- Robots can’t occupy the same place *(low priority)*
- Robots can’t be moved by other robots, e.g. push each other
- Commands must be directed to a specific robot
- Robots can be placed any number of times, i.e. picked up and placed by a giant

## Run

`$ ant run`

## Tests

### Unit

`$ ant`

### Integration

`$ ant runIntegration`

### All

`$ ant runAllTests`