package robot.application.command;

import org.junit.Test;

import java.util.Arrays;

import static junit.framework.Assert.assertEquals;

public class CommandParserTest {

    @Test
    public void returnsMoveWhenGivenMoveCommand() {
        CommandParser parser = new CommandParser();
        assertEquals(new MoveCommand().name(), parser.parse("move").name());
    }

    @Test
    public void returnsLeftWhenGivenLeftCommand() {
        CommandParser parser = new CommandParser();
        assertEquals(new TurnLeftCommand().name(), parser.parse("left").name());
    }

    @Test
    public void returnsRightWhenGivenRightCommand() {
        CommandParser parser = new CommandParser();
        assertEquals(new TurnRightCommand().name(), parser.parse("right").name());
    }

    @Test
    public void commandNamesAreAllDifferent() {
        CommandParser parser = new CommandParser();
        RobotCommand[] commands = parser.supportedCommands();
        long count = Arrays.stream(commands).map(RobotCommand::name).distinct().count();
        assertEquals(commands.length, count);
    }

    @Test
    public void commandNamesAreNotNull() {
        CommandParser parser = new CommandParser();
        RobotCommand[] commands = parser.supportedCommands();
        long count = Arrays.stream(commands).filter(c -> c.name() == null).count();
        assertEquals(0, count);
    }

    @Test
    public void returnsReportWhenGivenReportCommand() {
        CommandParser parser = new CommandParser();
        assertEquals(new ReportCommand().name(), parser.parse("report").name());
    }

    @Test
    public void returnsNoCommandWhenGivenUnknownInput() {
        CommandParser parser = new CommandParser();
        assertEquals(new NoCommand().name(), parser.parse("hello").name());
    }

}

