package robot.application.command;

import org.junit.Test;
import robot.application.entity.Board;
import robot.application.entity.Robot;
import robot.application.logging.Logger;
import robot.application.logging.NullLogger;

import static org.junit.Assert.*;

public class CommandResultTest {

    @Test
    public void canCreate() {
        assertNotNull(new CommandResult(true, null));
    }

    @Test
    public void canCreateSuccessfulResult() {
        CommandResult result = new CommandResult(true, null);
        assertTrue(result.isSuccessful());
    }

    @Test
    public void canCreateFailedResult() {
        CommandResult result = new CommandResult(false, null);
        assertFalse(result.isSuccessful());
    }

    @Test
    public void canReturnRobotPayload() {
        Logger logger = new NullLogger();
        Board board = new Board(1,1);

        CommandResult result = new CommandResult(true, new Robot(board, logger));
        assertNotNull(result.result());
    }

}