package robot.application.command;

import org.junit.Test;
import robot.application.entity.Board;
import robot.application.entity.Direction;
import robot.application.entity.Robot;
import robot.application.logging.Logger;
import robot.application.logging.TestLogger;

import static org.junit.Assert.*;

public class PlaceCommandTest {
    private int x = 1;
    private int y = 1;
    private Logger logger = new TestLogger();
    private Direction direction = Direction.EAST;
    private Board board = new Board(4, 4);

    private PlaceCommand command = new PlaceCommand();

    @Test
    public void executeSetsX() throws Exception {
        Robot robot = new Robot(0, 0, Direction.NORTH, board, logger);
        command.setArguments(x, y, direction);
        command.execute(robot);

        assertSame(x, robot.getX());
    }

    @Test
    public void executeSetsY() throws Exception {
        Robot robot = new Robot(0, 0, Direction.NORTH, board, logger);
        command.setArguments(x, y, direction);
        command.execute(robot);

        assertSame(y, robot.getY());
    }

    @Test
    public void executeSetsDirection() throws Exception {
        Robot robot = new Robot(0, 0, Direction.NORTH, board, logger);
        command.setArguments(x, y, direction);
        command.execute(robot);

        assertSame(direction, robot.getF());
    }
}