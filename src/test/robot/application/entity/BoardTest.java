package robot.application.entity;

import org.junit.Test;

import static org.junit.Assert.*;

public class BoardTest {
    @Test
    public void isValidMoveReturnsTrue() {
        Board board = new Board(3, 3);
        assertTrue(board.isValidMove(2,1));
    }

    @Test
    public void isValidMoveReturnsFalse() {
        Board board = new Board(3, 3);
        assertFalse(board.isValidMove(4, 1));
    }

    @Test
    public void isValidMoveReturnsTrueForOrigin() {
        Board board = new Board(3, 3);
        assertTrue(board.isValidMove(0, 0));
    }

    @Test
    public void isValidMoveReturnsTrueForEdge() {
        Board board = new Board(3, 3);
        assertTrue(board.isValidMove(2, 2));
    }
}