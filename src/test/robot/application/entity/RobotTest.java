package robot.application.entity;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import robot.application.entity.Direction;
import robot.application.entity.Robot;
import robot.application.logging.TestLogger;

public final class RobotTest extends Assert {

    @Test
    public void moveNorth() {
        Board board = new Board(4, 4);
        Robot robot = new Robot(1, 0, Direction.NORTH, board, new TestLogger());
        robot.move();

        assertSame(robot.getY(), 1);
        assertSame(robot.getX(), 1);
    }

    @Test
    public void moveEast() {
        Board board = new Board(4, 4);
        Robot robot = new Robot(1, 0, Direction.EAST, board, new TestLogger());
        robot.move();

        assertSame(robot.getX(), 2);
        assertSame(robot.getY(), 0);
    }

    @Test
    public void moveWest() {
        Board board = new Board(4, 4);
        Robot robot = new Robot(1, 0, Direction.WEST, board, new TestLogger());
        robot.move();

        assertSame(robot.getX(), 0);
        assertSame(robot.getY(), 0);
    }

    @Test
    public void moveSouth() {
        Board board = new Board(4, 4);
        Robot robot = new Robot(1, 1, Direction.SOUTH, board, new TestLogger());
        robot.move();

        assertSame(robot.getY(), 0);
        assertSame(robot.getX(), 1);
    }

    @Test
    public void reportsPosition() {
        Board board = new Board(4, 4);
        Robot robot = new Robot(1, 2, Direction.SOUTH, board, new TestLogger());

        assertTrue("Reports position", robot.report().equals("Output: 1,2,SOUTH"));
    }

    @Test
    public void placesRobotOnInit() {
        Board board = new Board(4, 4);
        Robot robot = new Robot(1, 2, Direction.SOUTH, board, new TestLogger());

        assertSame(robot.getX(), 1);
        assertSame(robot.getY(), 2);
        assertSame(robot.getF(), Direction.SOUTH);
    }

}
