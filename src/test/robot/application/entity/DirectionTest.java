package robot.application.entity;

import org.junit.Test;

import static org.junit.Assert.*;

public class DirectionTest {
    @Test
    public void canCreate() {
        Direction d = Direction.get("east");
        assertSame(Direction.EAST, d);
    }

    @Test
    public void turnLeftFromEast() {
        Direction d = Direction.EAST;
        assertSame(Direction.NORTH, d.left());
    }

    @Test
    public void turnRightFromEast() {
        Direction d = Direction.EAST;
        assertSame(Direction.SOUTH, d.right());
    }

    @Test
    public void turnLeftFromWest() {
        Direction d = Direction.WEST;
        assertSame(Direction.SOUTH, d.left());
    }

    @Test
    public void turnRightFromWest() {
        Direction d = Direction.WEST;
        assertSame(Direction.NORTH, d.right());
    }

    @Test
    public void turnLeftFromNorth() {
        Direction d = Direction.NORTH;
        assertSame(Direction.WEST, d.left());
    }
}
