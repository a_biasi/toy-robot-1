package robot.application.service;

import org.junit.Assert;
import org.junit.Test;
import robot.application.RobotSimulation;
import robot.application.entity.Robot;
import robot.application.input.CollectionStreamProcessor;
import robot.application.input.InputProcessor;
import robot.application.input.ResourceStreamProcessor;

import java.util.ArrayList;

public class RobotSimulationIntegrationTest extends Assert {

    @Test
    public void testRun1() {
        assertEqualsInstructToRobot("/testNormal1.txt", "Output: 1,0,EAST");
    }

    @Test
    public void testRun2() {
        assertEqualsInstructToRobot("/testNormal2.txt", "Output: 0,0,WEST");
    }

    @Test
    public void testRun3() {
        assertEqualsInstructToRobot("/testNormal3.txt", "Output: 3,3,NORTH");
    }

    @Test
    public void testRun4() {
        String filePath = "/testNormal4.txt";
        InputProcessor fileInput = new ResourceStreamProcessor(filePath);
        RobotSimulation simulation = new RobotSimulation(fileInput);
        Robot robot = simulation.run();

        assertEquals("Test for: " + filePath, null, robot);
    }

    @Test
    public void testRun5() {
        assertEqualsInstructToRobot("/testNormal5.txt", "Output: 4,4,NORTH");
    }

    @Test
    public void testRun6() {
        assertEqualsInstructToRobot("/testNormal6.txt", "Output: 3,4,NORTH");
    }

    @Test
    public void testRun7() {
        assertEqualsInstructToRobot("/testNormal7.txt", "Output: 0,0,SOUTH");
    }

    @Test
    public void testRun8() {
        assertEqualsInstructToRobot("/testNormal8.txt", "Output: 0,3,WEST");
    }

    @Test
    public void testSimulationFromArray() {
        ArrayList<String> input = new ArrayList<>();
        input.add("PLACE 1,0,EAST");

        InputProcessor inputProcessor = new CollectionStreamProcessor(input);
        RobotSimulation simulation = new RobotSimulation(inputProcessor);
        Robot robot = simulation.run();

        assertEquals("Output: 1,0,EAST", robot.report());
    }

    private void assertEqualsInstructToRobot(String filePath, String expectedOutput) {
        InputProcessor fileInput = new ResourceStreamProcessor(filePath);
        RobotSimulation simulation = new RobotSimulation(fileInput);
        Robot robot = simulation.run();

        assertEquals("Test for: " + filePath, expectedOutput, robot.report());
    }
}
