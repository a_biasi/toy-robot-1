package robot.application.entity;

public class Board {
    private final int WIDTH;
    private final int HEIGHT;

    public Board(int width, int height) {
        this.WIDTH = width;
        this.HEIGHT = height;
    }

    public boolean isValidMove(int x, int y) {
        boolean isValidX = x >= 0 && x < this.WIDTH;
        boolean isValidY = y >= 0 && y < this.HEIGHT;

        return isValidX && isValidY;
    }
}
