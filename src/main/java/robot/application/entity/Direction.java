package robot.application.entity;

import java.util.HashMap;
import java.util.Map;

public enum Direction {
    NORTH {
        public Direction left() {
            return WEST;
        }

        public Direction right() {
            return EAST;
        }
    },
    EAST {
        public Direction left() {
            return NORTH;
        }

        public Direction right() {
            return SOUTH;
        }
    },
    SOUTH {
        public Direction left() {
            return EAST;
        }

        public Direction right() {
            return WEST;
        }
    },
    WEST {
        public Direction left() {
            return SOUTH;
        }

        public Direction right() {
            return NORTH;
        }
    };

    static final Map<String,Direction> map = new HashMap<>();
    static {
        for (Direction d : Direction.values())
            map.put(d.toString(), d);
    }

    public static Direction get(String name) {
        return map.get(name.toUpperCase());
    }

    public abstract Direction left();
    public abstract Direction right();
}
