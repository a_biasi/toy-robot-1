package robot.application.entity;

import robot.application.logging.Logger;

public final class Robot {
    public static final int MAX_INDEX = 4; //Suggested more than 4 to pass unit testing

    private final Board board;
    private Integer x;
    private Integer y;
    private Direction f;
    private Logger logger;

    private boolean shouldDisplayError = true;
    private static final String ROBOT_GOES_BEYOND_THE_EDGE_INSTRUCTION_IGNORE = "Robot goes beyond the edge. Instruction ignore!";

    public Robot(Integer x, Integer y, Direction f, Board board, Logger logger) {
        this.logger = logger;
        this.board = board;
        place(x, y, f);
    }

    public Robot(Board board, Logger logger) {
        this.board = board;
        this.logger = logger;
    }

    public Robot(Robot other) {
        this(other.x, other.y, other.f, other.board, other.logger);
    }

    public Robot move() {
        Robot currentBot = new Robot(this);

        if (f.equals(Direction.NORTH)) {
            y++;
        } else if (f.equals(Direction.EAST)) {
            x++;
        } else if (f.equals(Direction.SOUTH)) {
            y--;
        } else if (f.equals(Direction.WEST)) {
            x--;
        }

        if (!this.board.isValidMove(x, y)) {
            displayError();
            return currentBot;
        }

        return this;
    }

    private void displayError() {
        if (shouldDisplayError) {
            shouldDisplayError = false;
            logger.error(ROBOT_GOES_BEYOND_THE_EDGE_INSTRUCTION_IGNORE);
        }
    }

    public void left() {
        this.f = this.f.left();
    }


    public void right() {
        this.f = this.f.right();
    }

    public boolean place(int x, int y, Direction f) {
        if (!this.board.isValidMove(x, y)) {
            logger.error("The robot is placed beyond the edge of the table. Instruction ignored!");
            return false;
        }

        this.x = x;
        this.y = y;
        this.f = f;
        return true;
    }

    public String report() {
        return "Output: " + x + "," + y + "," + f.name();
    }

    public Integer getX() {
        return x;
    }

    public Integer getY() {
        return y;
    }

    public Direction getF() {
        return f;
    }
}
