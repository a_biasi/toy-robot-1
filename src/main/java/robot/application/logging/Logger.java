package robot.application.logging;

public interface Logger {
    void error(String message);
}
