package robot.application.logging;

public class SystemLogger implements Logger {
    @Override
    public void error(String message) {
        System.out.println(message);
    }
}
