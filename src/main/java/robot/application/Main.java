package robot.application;

import robot.application.input.InputProcessor;
import robot.application.input.MenuProcessingTypeSelector;
import robot.application.input.ProcessingTypeSelector;

import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        ProcessingTypeSelector selector = new MenuProcessingTypeSelector();
        Optional<InputProcessor> maybeInput = selector.select();

        maybeInput.ifPresent(inputProcessor -> {
            RobotSimulation simulation = new RobotSimulation(inputProcessor);
            simulation.run();
        });
    }
}
