package robot.application;

import robot.application.command.CommandParser;
import robot.application.command.CommandResult;
import robot.application.command.PlaceCommand;
import robot.application.command.RobotCommand;
import robot.application.entity.Board;
import robot.application.entity.Direction;
import robot.application.entity.Robot;
import robot.application.input.InputProcessor;
import robot.application.logging.Logger;
import robot.application.logging.SystemLogger;

public class RobotSimulation {
    private Board board;
    private InputProcessor input;
    private CommandParser commandParser = new CommandParser();
    private Logger systemLogger = new SystemLogger();

    public RobotSimulation(InputProcessor input) {
        this.input = input;
        this.board = new Board(5, 5);
    }

    public Robot run() {
        Robot robot = null;
        String inputInstruction;

        boolean isPlaced = false;

        while((inputInstruction = this.input.readLine()) != null) {
            if (inputInstruction.equalsIgnoreCase("EXIT"))
                break;

            RobotCommand command = this.parseCommand(inputInstruction);

            if (!command.name().equals("Place")) {
                if (isPlaced) {
                    robot = command.execute(robot).result();
                }

                continue;
            }

            CommandResult result = command.execute(new Robot(this.board, systemLogger));

            if (result.isSuccessful()) {
                isPlaced = true;
                robot = result.result();
            }
        }

        this.input.close();
        return robot;
    }

    private RobotCommand parseCommand(String strLine) {
        String[] commandParts = strLine.split(" ");
        RobotCommand command = commandParser.parse(commandParts[0]);

        if (command.name().equals("Place")) {
            PlaceCommand placeCommand = (PlaceCommand) command;

            String[] placeArgs = commandParts[1].split(",");
            int x = Integer.parseInt(placeArgs[0]);
            int y = Integer.parseInt(placeArgs[1]);
            Direction d = Direction.get(placeArgs[2]);
            placeCommand.setArguments(x, y, d);
        }

        return command;
    }
}
