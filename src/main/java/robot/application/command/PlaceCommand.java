package robot.application.command;

import robot.application.entity.Direction;
import robot.application.entity.Robot;

public class PlaceCommand implements RobotCommand {
    private int x, y;
    private Direction direction;

    public void setArguments(int x, int y, Direction d) {
        this.x = x;
        this.y = y;
        this.direction = d;
    }

    @Override
    public CommandResult execute(Robot robot) {
        boolean result = robot.place(this.x, this.y, this.direction);
        return new CommandResult(result, robot);
    }

    @Override
    public String name() {
        return "Place";
    }
}
