package robot.application.command;

import robot.application.entity.Robot;

public interface RobotCommand {
    CommandResult execute(Robot robot);

    String name();
}
