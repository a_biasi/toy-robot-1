package robot.application.command;

import robot.application.entity.Robot;

public class ReportCommand implements RobotCommand {
    @Override
    public CommandResult execute(Robot robot) {
        System.out.println(robot.report());
        return new CommandResult(true, robot);
    }

    @Override
    public String name() {
        return "Report";
    }
}
