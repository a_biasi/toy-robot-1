package robot.application.command;

import robot.application.entity.Robot;

public class NoCommand implements RobotCommand {
    @Override
    public CommandResult execute(Robot robot) {
        return new CommandResult(true, robot);
    }

    @Override
    public String name() {
        return "Nope";
    }
}
