package robot.application.command;

import robot.application.entity.Robot;

public class CommandResult {
    private boolean successful;
    private Robot robot;

    public CommandResult(boolean successful, Robot robot) {
        this.successful = successful;
        this.robot = robot;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public Robot result() {
        return this.robot;
    }

}
