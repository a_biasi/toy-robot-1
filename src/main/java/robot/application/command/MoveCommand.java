package robot.application.command;

import robot.application.entity.Robot;

public class MoveCommand implements RobotCommand {
    @Override
    public CommandResult execute(Robot robot) {
        Robot movedRobot = robot.move();
        return new CommandResult(true, movedRobot);
    }

    public String name() {
        return "Move";
    }
}
