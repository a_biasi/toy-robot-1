package robot.application.command;

import robot.application.entity.Robot;

public class TurnLeftCommand implements RobotCommand {
    @Override
    public CommandResult execute(Robot robot) {
        robot.left();
        return new CommandResult(true, robot);
    }

    @Override
    public String name() {
        return "Left";
    }
}
