package robot.application.command;

import java.util.HashMap;
import java.util.Map;

public class CommandParser {

    private Map<String, RobotCommand> commandMap;
    private RobotCommand[] commands = new RobotCommand[]{
            new MoveCommand(),
            new TurnLeftCommand(),
            new TurnRightCommand(),
            new ReportCommand(),
            new PlaceCommand(),
            new NoCommand()
    };

    public CommandParser() {
        commandMap = new HashMap<>();
        for (RobotCommand r : commands) {
            commandMap.put(r.name(), r);
            commandMap.put(r.name().toUpperCase(), r);
            commandMap.put(r.name().toLowerCase(), r);
        }
    }

    public RobotCommand parse(String input) {
        return commandMap.getOrDefault(input, new NoCommand());
    }

    public RobotCommand[] supportedCommands() {
        return commands;
    }

}
