package robot.application.command;

import robot.application.entity.Robot;

public class TurnRightCommand implements RobotCommand {
    @Override
    public CommandResult execute(Robot robot) {
        robot.right();
        return new CommandResult(true, robot);
    }

    @Override
    public String name() {
        return "Right";
    }
}
