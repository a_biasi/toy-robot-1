package robot.application.input;

import java.util.Optional;

public interface ProcessingTypeSelector {
    Optional<InputProcessor> select();
}
