package robot.application.input;

import java.util.ArrayList;
import java.util.Iterator;

public class CollectionStreamProcessor implements InputProcessor {
    private Iterator<String> input;

    public CollectionStreamProcessor(ArrayList<String> input) {
        this.input = input.iterator();
    }

    @Override
    public String readLine() {
        if (this.input.hasNext()) {
            return this.input.next();
        }

        return null;
    }

    @Override
    public void close() {
    }
}
