package robot.application.input;

import robot.application.logging.Logger;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileInputProcessor implements InputProcessor {
    private final Logger logger;
    private BufferedReader input;
    private boolean inputAvailable;

    public FileInputProcessor(String fileName, Logger logger) {
        this.logger = logger;

        try {
            this.input = new BufferedReader(new FileReader(fileName));
            this.inputAvailable = true;
        } catch (FileNotFoundException e) {
            this.inputAvailable = false;
        }
    }

    @Override
    public String readLine() {
        if(!this.inputAvailable) {
            this.logger.error("Unable to read file.");
            return null;
        }

        try {
            return this.input.readLine();
        } catch (IOException e) {
            this.logger.error("Unable to read file.");
            return null;
        }
    }

    @Override
    public void close() {

    }
}
