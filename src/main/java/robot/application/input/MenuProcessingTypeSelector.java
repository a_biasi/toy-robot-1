package robot.application.input;

import robot.application.logging.Logger;
import robot.application.logging.SystemLogger;

import java.util.Optional;
import java.util.Scanner;

public class MenuProcessingTypeSelector implements ProcessingTypeSelector {
    private Logger logger = new SystemLogger();
    private Scanner input = new Scanner(System.in);

    @Override
    public Optional<InputProcessor> select() {
        String selectedOption = this.readMenuOption();
        return Optional.ofNullable(this.getInputProcessor(selectedOption));
    }

    private String readMenuOption() {
        System.out.println("\nPlease select the menu below");
        System.out.println("\t[1] Input instructions to Robot:");
        System.out.println("\t[2] Input instructions by importing file");
        System.out.println("\t[3] Exit");

        return this.input.nextLine();
    }

    private InputProcessor getInputProcessor(String option) {
        switch(option) {
            case "1":
                return this.getCommandInputProcessor();
            case "2":
                String filePath = this.requestFilePath();
                return this.getFileInputProcessor(filePath);
            case "0":
            default:
                return this.logAndReturnEmpty();
        }
    }

    private InputProcessor getCommandInputProcessor() {
        return new CommandLineInputProcessor();
    }

    private InputProcessor getFileInputProcessor(String filePath) {
        return new FileInputProcessor(filePath, this.logger);
    }

    private String requestFilePath() {
        System.out.println("Please input file path: ");
        return this.input.nextLine();
    }

    private InputProcessor logAndReturnEmpty() {
        this.logger.error("Invalid option.");
        return null;
    }
}
