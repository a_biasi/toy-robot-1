package robot.application.input;

public interface InputProcessor {
    String readLine();
    void close();
}
