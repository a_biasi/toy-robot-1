package robot.application.input;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CommandLineInputProcessor implements InputProcessor {
    private BufferedReader input;

    public CommandLineInputProcessor() {
        this.input = new BufferedReader(new InputStreamReader(System.in));
    }

    @Override
    public String readLine() {
        try {
            return this.input.readLine();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void close() {
        try {
            this.input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
