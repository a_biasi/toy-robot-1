package robot.application.input;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ResourceStreamProcessor implements InputProcessor {
    private BufferedReader input;

    public ResourceStreamProcessor(String resourceName) {
        this.input = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(resourceName)));
    }

    @Override
    public String readLine() {
        try {
            return this.input.readLine();
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public void close() {
        try {
            this.input.close();
        } catch (IOException e) {

        }
    }
}
